# i_rl = deconvRL(imblur, kernel, non_uniform, ...)
#       for uniform blur, with non_uniform = 0
#
# i_rl = deconvRL(imblur, kernel, non_uniform, theta_list, Kblurry, ...)
#       for non-uniform blur, with non_uniform = 1
#
# Additional arguments, in any order:
#   ... , 'forward_saturation', ...    use forward model for saturation
#   ... , 'prevent_ringing', ...       split and re-combine updates to reduce ringing
#   ... , 'sat_thresh', T, ...         clipping level for forward model of saturation (default is 1.0)
#   ... , 'sat_smooth', a, ...         smoothness parameter for forward model (default is 50)
#   ... , 'num_iters', n, ...          number of iterations (default is 50)
#   ... , 'init', im_init, ...         initial estimate of deblurred image (default is blurry image)
#   ... , 'mask', mask, ...            binary mask of blurry pixels to use -- 1=use, 0=discard -- (default is all blurry pixels, ie. 1 everywhere)

#	Author:		Oliver Whyte <oliver.whyte@ens.fr>
#	Date:		November 2011
#	Copyright:	2011, Oliver Whyte
#	Reference:  O. Whyte, J. Sivic and A. Zisserman. "Deblurring Shaken and Partially Saturated Images". In Proc. CPCV Workshop at ICCV, 2011.
#	URL:		http://www.di.ens.fr/willow/research/saturation/

import numpy as np
    
def deconvRL(imblur = None,kernel = None,non_uniform = None,varargin = None): 
    if len(varargin) < 3:
        non_uniform = 0
    
    # Discard small kernel elements for speed
    kernel[kernel < np.amax[kernel] / 100] = 0
    kernel = kernel / sum(kernel)
    # Parse varargin
    if non_uniform:
        theta_list = varargin[0]
        Kblurry = varargin[2]
        varargin = varargin(np.arange(3,end()+1))
        # Remove the zero elements of kernel
        use_rotations = kernel != 0
        kernel = kernel(use_rotations)
        theta_list = theta_list(:,use_rotations)
    
    params = parseArgs(varargin)
    # Get image size
    h,w,channels = imblur.shape
    # Calculate padding based on blur kernel size
    if non_uniform:
        pad,Kblurry = calculatePadding(np.array([h,w]),non_uniform,theta_list,Kblurry)
    else:
        pad = calculatePadding(np.array([h,w]),non_uniform,kernel)
    
    # Pad blurry image by replication to handle edge effects
    imblur = padImage(imblur,pad,'replicate')
    # Get new image size
    h,w,channels = imblur.shape
    # Define blur functions depending on blur type
    if non_uniform:
        Ksharp = Kblurry
        blurfn = lambda im = None: apply_blur_kernel_mex(double(im),np.array([h,w]),Ksharp,Kblurry,- theta_list,kernel,0,non_uniform)
        conjfn = lambda im = None: apply_blur_kernel_mex(double(im),np.array([h,w]),Kblurry,Ksharp,theta_list,kernel,0,non_uniform)
        dilatefn = lambda im = None: np.amin(apply_blur_kernel_mex(double(im),np.array([h,w]),Ksharp,Kblurry,- theta_list,np.ones((kernel.shape,kernel.shape)),0,non_uniform),1)
    else:
        # blurfn  = @(im) imfilter(im,kernel,'conv');
# conjfn  = @(im) imfilter(im,kernel,'corr');
# dilatefn = @(im) min(imfilter(im,double(kernel~=0),'conv'), 1);
        kfft = psf2otf(kernel,np.array([h,w]))
        k1fft = psf2otf(double(kernel != 0),np.array([h,w]))
        blurfn = lambda im = None: ifft2(bsxfun(times,fft2(im),kfft),'symmetric')
        conjfn = lambda im = None: ifft2(bsxfun(times,fft2(im),conj(kfft)),'symmetric')
        dilatefn = lambda im = None: np.amin(ifft2(bsxfun(times,fft2(im),k1fft),'symmetric'),1)
    
    # Mask of "good" blurry pixels
    mask = np.zeros((h,w,channels))
    mask[np.arange[pad[1] + 1,h - pad[2]+1],np.arange[pad[3] + 1,w - pad[4]+1],:] = params.mask
    # Initialise sharp image
    if isfield(params,'init'):
        i_rl = padImage(double(params.init),pad,'replicate')
    else:
        i_rl = imblur
    
    print('%d iterations: ' % (params.num_iters))
    # Some fixed filters for dilation and smoothing
    dilate_radius = 3
    dilate_filter = bsxfun(plus,(np.arange(- dilate_radius,dilate_radius+1)) ** 2,np.transpose((np.arange(- dilate_radius,dilate_radius+1))) ** 2) <= eps + dilate_radius ** 2
    smooth_filter = fspecial('gaussian',np.array([21,21]),3)
    # Main algorithm loop
    for iter in np.arange(1,params.num_iters+1).reshape(-1):
        # Apply the linear forward model first (ie. compute A*f)
        val_linear = np.amax(blurfn(i_rl),0)
        # Apply non-linear response if required
        if params.forward_saturation:
            val_nonlin,grad_nonlin = saturate(val_linear,params.sat_thresh,params.sat_smooth)
        else:
            val_nonlin = val_linear
            grad_nonlin = 1
        # Compute the raw error ratio for the current estimate of the sharp image
        error_ratio = imblur / np.amax(val_nonlin,eps)
        error_ratio_masked_nonlin = np.multiply(np.multiply((error_ratio - 1),mask),grad_nonlin)
        if params.prevent_ringing:
            # Find hard-to-estimate pixels in sharp image (set S in paper)
            S_mask = double(imdilate(i_rl >= 0.9,dilate_filter))
            # Find the blurry pixels NOT influenced by pixels in S (set V in paper)
            V_mask = 1 - dilatefn(S_mask)
            # Apply conjugate blur function (ie. multiply by A')
            update_ratio_U = conjfn(np.multiply(error_ratio_masked_nonlin,V_mask)) + 1
            update_ratio_S = conjfn(error_ratio_masked_nonlin) + 1
            # Blur the mask of hard-to-estimate pixels for recombining without artefacts
            weights = imfilter(S_mask,smooth_filter)
            # Combine updates for the two sets into a single update
# update_ratio = update_ratio_S.*weights + update_ratio_U.*(1-weights);
            update_ratio = update_ratio_U + np.multiply((update_ratio_S - update_ratio_U),weights)
        else:
            # Apply conjugate blur function (ie. multiply by A')
            update_ratio = conjfn(error_ratio_masked_nonlin) + 1
        # Avoid negative updates, which cause trouble
        update_ratio = np.amax(update_ratio,0)
        if np.any(np.isnan(update_ratio)):
            raise Exception('NaNs in update ratio')
        # Apply update ratio
        i_rl = np.multiply(update_ratio,i_rl)
        print('%d ' % (iter))
    
    print('\n' % ())
    # Remove padding on output image
    i_rl = padImage(i_rl,- pad)
    return i_rl
    
    # =====================================================================================
    
    
def parseArgs(args = None,params = None): 
    if len(varargin) < 2:
        params = struct('num_iters',50,'forward_saturation',False,'prevent_ringing',False,'sat_thresh',1,'sat_smooth',50,'mask',1)
    
    if not len(args)==0 :
        if 'num_iters' == args[0]:
            params.num_iters = args[2]
            args = args(np.arange(3,end()+1))
        else:
            if 'sat_thresh' == args[0]:
                params.sat_thresh = args[2]
                args = args(np.arange(3,end()+1))
            else:
                if 'sat_smooth' == args[0]:
                    params.sat_smooth = args[2]
                    args = args(np.arange(3,end()+1))
                else:
                    if 'forward_saturation' == args[0]:
                        params.forward_saturation = True
                        args = args(np.arange(2,end()+1))
                    else:
                        if 'prevent_ringing' == args[0]:
                            params.prevent_ringing = True
                            args = args(np.arange(2,end()+1))
                        else:
                            if 'init' == args[0]:
                                params.init = args[2]
                                args = args(np.arange(3,end()+1))
                            else:
                                if 'mask' == args[0]:
                                    params.mask = args[2]
                                    args = args(np.arange(3,end()+1))
                                else:
                                    raise Exception('Invalid argument')
        # Recursively parse remaining arguments
        params = parseArgs(args,params)
    
    return params
    
    # ============================================================================
    
    
def saturate(x = None,t = None,a = None): 
    if a == inf:
        val,grad = sharpsaturate(x,t)
    else:
        # Adapted from: C. Chen and O. L. Mangasarian. ``A Class of Smoothing Functions
#               for Nonlinear and Mixed Complementarity Problems''.
#               Computational Optimization and Applications, 1996.
        one_p_exp = 1 + np.exp(- a * (t - x))
        val = x - 1 / a * np.log(one_p_exp)
        grad = 1 / one_p_exp
    
    return val,grad
    
    # ============================================================================
    
    
def sharpsaturate(x = None,t = None): 
    val = x
    grad = np.ones((x.shape,x.shape))
    mask = x > t
    val[mask] = t
    grad[mask] = 0
    return val,grad
    
    return i_rl