# pad = calculatePadding(image_size,non_uniform = 0,kernel)
# pad = calculatePadding(image_size,non_uniform = 1,theta_list,Kinternal)
#       where pad = [top, bottom, left, right]

#	Author:		Oliver Whyte <oliver.whyte@ens.fr>
#	Date:		November 2011
#	Copyright:	2011, Oliver Whyte
#	Reference:  O. Whyte, J. Sivic and A. Zisserman. "Deblurring Shaken and Partially Saturated Images". In Proc. CPCV Workshop at ICCV, 2011.
#	URL:		http://www.di.ens.fr/willow/research/saturation/

import numpy as np
    
def calculatePadding(image_size = None,non_uniform = None,theta_list = None,Kinternal = None): 
    h_sharp = image_size(1)
    w_sharp = image_size(2)
    if non_uniform:
        # Calculate padding
        im_corners = np.array([[1,1,w_sharp,w_sharp],[1,h_sharp,h_sharp,1],[1,1,1,1]])
        pad_replicate_t = 0
        pad_replicate_b = 0
        pad_replicate_l = 0
        pad_replicate_r = 0
        # for each non-zero in the kernel...
        for i in np.arange(1,theta_list.shape[2-1]+1).reshape(-1):
            # back proect corners of blurry image to see how far out we need to pad
# H = Ksharp*expm(crossmatrix(-theta_list(:,i)))*inv(Kblurry);
            H = Kinternal * expm(crossmatrix(- theta_list(:,i))) * inv(Kinternal)
            projected_corners_sharp = hnormalise(H * im_corners)
            offsets = np.abs(projected_corners_sharp - im_corners)
            if offsets(1,1) > pad_replicate_l:
                pad_replicate_l = np.ceil(offsets(1,1))
            if offsets(1,2) > pad_replicate_l:
                pad_replicate_l = np.ceil(offsets(1,2))
            if offsets(1,3) > pad_replicate_r:
                pad_replicate_r = np.ceil(offsets(1,3))
            if offsets(1,4) > pad_replicate_r:
                pad_replicate_r = np.ceil(offsets(1,4))
            if offsets(2,1) > pad_replicate_t:
                pad_replicate_t = np.ceil(offsets(2,1))
            if offsets(2,2) > pad_replicate_b:
                pad_replicate_b = np.ceil(offsets(2,2))
            if offsets(2,3) > pad_replicate_t:
                pad_replicate_t = np.ceil(offsets(2,3))
            if offsets(2,4) > pad_replicate_b:
                pad_replicate_b = np.ceil(offsets(2,4))
        # Adjust calibration matrices to take account padding
        Kinternal = htranslate(np.array([[pad_replicate_l],[pad_replicate_t]])) * Kinternal
    else:
        kernel = theta_list
        pad_replicate_t = np.ceil((kernel.shape[1-1] - 1) / 2)
        pad_replicate_b = int(np.floor((kernel.shape[1-1] - 1) / 2))
        pad_replicate_l = np.ceil((kernel.shape[2-1] - 1) / 2)
        pad_replicate_r = int(np.floor((kernel.shape[2-1] - 1) / 2))
        Kinternal = []
    
    w_sharp = w_sharp + pad_replicate_l + pad_replicate_r
    h_sharp = h_sharp + pad_replicate_t + pad_replicate_b
    # top, bottom, left, right
    pad_replicate = np.array([pad_replicate_t,pad_replicate_b,pad_replicate_l,pad_replicate_r])
    return pad_replicate,Kinternal