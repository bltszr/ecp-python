import numpy as np
    
def psnr1(x = None,y = None): 
    # psnr - compute the Peack Signal to Noise Ratio, defined by :
#       PSNR(x,y) = 10*log10( max(max(x),max(y))^2 / |x-y|^2 ).
    
    #   p = psnr(x,y);
    
    #   Copyright (c) 2004 Gabriel Peyr??
    
    #d = mean( mean( (x(:)-y(:)).^2 ) );
    d = comp_upto_shift1(x,y) / (x.shape[1-1] * x.shape[2-1])
    m1 = np.amax(np.abs(x))
    m2 = np.amax(np.abs(y))
    m = np.amax(m1,m2)
    p = 10 * log10(m ** 2 / d)
    return p