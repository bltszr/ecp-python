import numpy as np
from cho_code.opt_fft_size import opt_fft_size
import matplotlib.pyplot as plt
from scipy.fft import dst, idst
import sys

def wrap_boundary_liu(img = None,img_size = None): 
    # wrap_boundary_liu.m
    
    #   pad image boundaries such that image boundaries are circularly smooth
    
    #     written by Sunghyun Cho (sodomau@postech.ac.kr)
    
    # This is a variant of the method below:
#   Reducing boundary artifacts in image deconvolution
#     Renting Liu, Jiaya Jia
#     ICIP 2008
    try:
        H,W,Ch = img.shape
    except ValueError:
        H, W = img.shape
        Ch = 1
    img_size = img_size.astype(np.int)
    H_w = img_size[0] - H
    W_w = img_size[1] - W
    ret = np.zeros((img_size[0],img_size[1],Ch))
    for ch in np.arange(0,Ch).reshape(-1):
        alpha = 1
#        HG = img[:,:,ch]
        HG = img[:,:]
        r_A = np.zeros((alpha * 2 + H_w,W))
        r_A[0:alpha,:] = HG[-alpha:,:]
        r_A[-alpha:,:] = HG[0:alpha,:]
        a = ((np.arange(0,H_w)) - 1) / (H_w - 1)
        r_A[alpha:-alpha, 0] = (1 - a) * r_A[alpha-1,0] + a * r_A[-alpha,0]
        r_A[alpha:-alpha, -1] = (1 - a) * r_A[alpha-1,-1] + a * r_A[-alpha,-1]
        
#        sys.exit(0)
        A2 = solve_min_laplacian(r_A[alpha-1:r_A.shape[0]-alpha,:])
        r_A[alpha-1:r_A.shape[0]-alpha,:] = A2
        A = r_A
        
        r_B = np.zeros((H,alpha * 2 + W_w))
        r_B[:,0:alpha] = HG[:,-alpha:]
        r_B[:,-alpha:] = HG[:,0:alpha]
        a = ((np.arange(0,W_w)) - 1) / (W_w - 1)
        r_B[0,alpha:-alpha] = (1 - a) * r_B[0,alpha-1] + a * r_B[0,-alpha]
        r_B[-1,alpha:-alpha] = (1 - a) * r_B[-1,alpha-1] + a * r_B[-1,-alpha]
        
        B2 = solve_min_laplacian(r_B[:,alpha-1:r_B.shape[1]-alpha])
        r_B[:,alpha-1:r_B.shape[1]-alpha] = B2
        B = r_B
       
        r_C = np.zeros((alpha * 2 + H_w,alpha * 2 + W_w))
        r_C[0:alpha,:] = B[-alpha:,:]
        r_C[-alpha:,:] = B[0:alpha,:]
        r_C[:,0:alpha] = A[:,-alpha:]
        r_C[:,-alpha:] = A[:,0:alpha]
        
        C2 = solve_min_laplacian(r_C[alpha-1:r_C.shape[0]-alpha,
                                     alpha-1:r_C.shape[1]-alpha])
        r_C[alpha-1:r_C.shape[0]-alpha,
            alpha-1:r_C.shape[1]-alpha] = C2
        C = r_C
        
        A = A[alpha-1:-alpha-1]
        B = B[:, alpha:-alpha]
        C = C[alpha:-alpha, alpha:-alpha]
        
        # ret[:,:,ch] = np.array([[img[:,:,ch],B],[A,C]])
        ret = np.block([[img[:,:],B],[A,C]])
    return ret
    
    
def solve_min_laplacian(boundary_image = None): 
    # function [img_direct] = poisson_solver_function(gx,gy,boundary_image)
# Inputs; Gx and Gy -> Gradients
# Boundary Image -> Boundary image intensities
# Gx Gy and boundary image should be of same size
    H,W = boundary_image.shape
    # Laplacian
    f = np.zeros((H,W))
    # boundary image contains image intensities at boundaries
    boundary_image[1:-1, 1:-1] = 0
    j1, j2 = 1, H-1
    k1, k2 = 1, W-1
    f_bp = np.zeros((H,W))
    f_bp[j1:j2,k1:k2] = - 4 * (boundary_image[j1:j2,k1:k2])# + boundary_image[j,k + 1] + boundary_image[j,k - 1] + boundary_image[j - 1,k] + boundary_image[j + 1,k])
    
    #f1 = f - reshape(f_bp,H,W); # subtract boundary points contribution
    f1 = f - f_bp
    
    
    # DST Sine Transform algo starts here
    f2 = f1[1:-1, 1:-1]
    
    # compute sine tranform
    tt = dst(f2)
    f2sin = np.transpose(dst(np.transpose(tt)))
    
    # compute Eigen Values
    x,y = np.meshgrid(np.arange(0,W - 2),np.arange(0,H - 2))
    denom = (2 * np.cos(np.pi * x / (W - 1)) - 2) + (2 * np.cos(np.pi * y / (H - 1)) - 2)
    # divide
    f3 = f2sin / denom
    
    # compute Inverse Sine Transform
    tt = idst(f3)
    
    img_tt = np.transpose(idst(np.transpose(tt)))
    
    # put solution in inner points; outer points obtained from boundary image
    img_direct = boundary_image
    img_direct[1:-1, 1:-1] = 0
    img_direct[1:-1, 1:-1] = img_tt
    return img_direct
