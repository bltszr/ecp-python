import numpy as np


opt_fft_size_LUT = []
def opt_fft_size(n = None): 
    # opt_fft_size.m
    
    #   compute an optimal data length for Fourier transforms
    
    #     written by Sunghyun Cho (sodomau@postech.ac.kr)
    
    global opt_fft_size_LUT
    LUT_size = 4096
    if len(opt_fft_size_LUT)==0:
        print('generate opt_fft_size_LUT\n' % ())
        opt_fft_size_LUT = np.zeros(LUT_size)
        e2 = 1
        while e2 < LUT_size:

            e3 = e2
            while e3 < LUT_size:

                e5 = e3
                while e5 < LUT_size:

                    e7 = e5
                    while e7 < LUT_size:

                        if e7 < LUT_size:
                            opt_fft_size_LUT[e7] = e7
                        if e7 * 11 < LUT_size:
                            opt_fft_size_LUT[e7 * 11] = e7 * 11
                        if e7 * 13 < LUT_size:
                            opt_fft_size_LUT[e7 * 13] = e7 * 13
                        e7 = e7 * 7

                    e5 = e5 * 5

                e3 = e3 * 3

            e2 = e2 * 2

        nn = 0
        for i in np.arange(LUT_size-1,0,-1).reshape(-1):
            if opt_fft_size_LUT[i] != 0:
                nn = i
            else:
                opt_fft_size_LUT[i] = nn
    
    m = np.zeros(n.shape)
    for c in np.arange(0,np.asarray(n).size).reshape(-1):
        nn = n[c]
        if nn < LUT_size:
            m[c] = opt_fft_size_LUT[nn]
        else:
            m[c] = - 1
    
    return m
