import matplotlib.pyplot as plt
import numpy as np
from bilateral_filter import bilateral_filter
from scipy.signal import convolve2d as conv2
import sys

def threshold_pxpy_v1(latent = None,psf_size = None, threshold = None): 
    # mask indicates region with gradients. outside of mask shud be smooth...
    
    if threshold is None:
        threshold = 0
        b_estimate_threshold = True
    else:
        b_estimate_threshold = False
    
    #{
#    denoised = bilateral_filter(latent,2.0,param.sigma,'replicate',2)
#    if opts.verbose:
#        plt.figure(1992)
#        imshow(denoised)
#        plt.title('denoised')   
    #}
    denoised = latent
    ##
# derivative filters
    dx = np.array([[- 1,1],[0,0]])
    dy = np.array([[- 1,0],[1,0]])
    ##
# px = imfilter(denoised, [0 -1 1], 'same', 'replicate');
# py = imfilter(denoised, [0;-1;1], 'same', 'replicate');
    px = conv2(denoised,dx,'valid')
    py = conv2(denoised,dy,'valid')
    pm = px ** 2 + py ** 2
    # if this is the first prediction, then we need to find an appropriate
    # threshold value by building a histogram of gradient magnitudes
    if b_estimate_threshold:
        pd = np.arctan(py / px)
        pm_steps = np.arange(0,2+6e-05,6e-05)
        H1 = np.cumsum(np.flipud(np.digitize(pm[np.logical_and(pd >= 0, pd < np.pi / 4)],pm_steps)))
        H2 = np.cumsum(np.flipud(np.digitize(pm[np.logical_and(pd >= np.pi / 4, pd < np.pi / 2)],pm_steps)))
        H3 = np.cumsum(np.flipud(np.digitize(pm[np.logical_and(pd >= - np.pi / 4, pd < 0)],pm_steps)))
        H4 = np.cumsum(np.flipud(np.digitize(pm[np.logical_and(pd >= - np.pi / 2, pd < - np.pi / 4)],pm_steps)))
        th = np.amax(np.array([np.amax(psf_size) * 20,10]))
        for t in np.arange(1,np.asarray(pm_steps).size+1).reshape(-1):
            min_h = np.amin(np.array([H1[t],H2[t],H3[t],H4[t]]))
            if min_h >= th:
                threshold = pm_steps[len(pm_steps) - t]
                break
    # thresholding
    m = pm < threshold
    # TOL = (2**-256)
    while np.all(m == 1):
        # prev_thresh = threshold
        threshold = threshold * 0.81
        m = pm < threshold
        # if abs(prev_thresh-threshold) < TOL:
        #    break

    px[m] = 0
    py[m] = 0
    # # update prediction parameters
# threshold = threshold * 0.9;
## my modification
    if b_estimate_threshold:
        threshold = threshold
    else:
        threshold = threshold / 1.1
    
    return px,py,threshold
