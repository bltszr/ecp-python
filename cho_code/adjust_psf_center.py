import numpy as np
    
def adjust_psf_center(psf = None): 
    X,Y = np.meshgrid(np.arange(1,psf.shape[2-1]+1),np.arange(1,psf.shape[1-1]+1))
    xc1 = sum2(np.multiply(psf,X))
    yc1 = sum2(np.multiply(psf,Y))
    xc2 = (psf.shape[2-1] + 1) / 2
    yc2 = (psf.shape[1-1] + 1) / 2
    xshift = np.round(xc2 - xc1)
    yshift = np.round(yc2 - yc1)
    psf = warpimage(psf,np.array([[1,0,- xshift],[0,1,- yshift]]))
    
def sum2(arr = None): 
    val = sum(arr)
    ##
# M should be an inverse transform!
    
def warpimage(img = None,M = None): 
    if img.shape[3-1] == 3:
        warped[:,:,1] = warpProjective2(img(:,:,1),M)
        warped[:,:,2] = warpProjective2(img(:,:,2),M)
        warped[:,:,3] = warpProjective2(img(:,:,3),M)
        warped[np.isnan[warped]] = 0
    else:
        warped = warpProjective2(img,M)
        warped[np.isnan[warped]] = 0
    
    ##
    
def warpProjective2(im = None,A = None): 
    
    # function result = warpProjective2(im,A)
    
    # im: input image
# A: 2x3 affine transform matrix or a 3x3 matrix with [0 0 1]
# for the last row.
# if a transformed point is outside of the volume, NaN is used
    
    # result: output image, same size as im
    
    if (A.shape[1-1] > 2):
        A = A(np.arange(1,2+1),:)
    
    # Compute coordinates corresponding to input
# and transformed coordinates for result
    x,y = np.meshgrid(np.arange(1,im.shape[2-1]+1),np.arange(1,im.shape[1-1]+1))
    coords = np.array([[np.transpose(x)],[np.transpose(y)]])
    homogeneousCoords = np.array([[coords],[np.ones((1,np.prod(im.shape)))]])
    warpedCoords = A * homogeneousCoords
    xprime = warpedCoords(1,:)
    
    yprime = warpedCoords(2,:)
    
    result = interp2(x,y,im,xprime,yprime,'linear')
    result = np.reshape(result, tuple(im.shape), order="F")
    return result
    return psf