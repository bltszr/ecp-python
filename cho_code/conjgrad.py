import numpy as np
    
def conjgrad(x = None,b = None,maxIt = None,tol = None,Ax_func = None,func_param = None,visfunc = None): 
    # conjgrad.m
    
    #   Conjugate gradient optimization
    
    #     written by Sunghyun Cho (sodomau@postech.ac.kr)
    
    r = b - Ax_func(x,func_param)
    p = r
    rsold = sum(np.multiply(r,r))
    for iter in np.arange(1,maxIt+1).reshape(-1):
        Ap = Ax_func(p,func_param)
        alpha = rsold / sum(np.multiply(p,Ap))
        x = x + alpha * p
        if ('visfunc' is not None):
            visfunc(x,iter,func_param)
        r = r - alpha * Ap
        rsnew = sum(np.multiply(r,r))
        if np.sqrt(rsnew) < tol:
            break
        p = r + rsnew / rsold * p
        rsold = rsnew
    
    return x
    
    return x