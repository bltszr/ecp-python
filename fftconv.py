import numpy as np
    
def fftconv(I = None,filt = None,b_otf = None): 
    if I.shape[3-1] == 3:
        H,W,ch = I.shape
        otf = psf2otf(filt,np.array([H,W]))
        cI[:,:,1] = fftconv(I(:,:,1),otf,True)
        cI[:,:,2] = fftconv(I(:,:,2),otf,True)
        cI[:,:,3] = fftconv(I(:,:,3),otf,True)
        return cI
    
    if ('b_otf' is not None) and b_otf == True:
        cI = real(ifft2(np.multiply(fft2(I),filt)))
    else:
        cI = real(ifft2(np.multiply(fft2(I),psf2otf(filt,I.shape))))
    
    return cI