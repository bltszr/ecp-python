import numpy as np
    
def assign_dark_channel_to_pixel(S = None,dark_channel_refine = None,dark_channel_index = None,patch_size = None): 
    ## assign dark channel value to image pixel
# The Code is created based on the method described in the following paper
#   [1] Jinshan Pan, Deqing Sun, Hanspteter Pfister, and Ming-Hsuan Yang,
#        Blind Image Deblurring Using Dark Channel Prior, CVPR, 2016.
    
    M,N,C = S.shape
    #outImge = zeros(M, N); #
    
    # pad original image
    padsize = int(np.floor(patch_size / 2))
    S_padd = padarray(S,np.array([padsize,padsize]),'replicate')
    # assign dark channel to pixel
    m = np.arange(1,M+1)
    for n in np.arange(1,N+1).reshape(-1):
        patch = S_padd(np.arange(m,(m + patch_size - 1)+1),np.arange(n,(n + patch_size - 1)+1),:)
        if not np.amin(patch)==dark_channel_refine(m,n) :
            patch[dark_channel_index[m,n]] = dark_channel_refine(m,n)
        for cc in np.arange(1,C+1).reshape(-1):
            S_padd[np.arange[m,[m + patch_size - 1]+1],np.arange[n,[n + patch_size - 1]+1],cc] = patch(:,:,cc)
    
    outImg = S_padd(np.arange(padsize + 1,end() - padsize+1),np.arange(padsize + 1,end() - padsize+1),:)
    ## boundary processing
    outImg[np.arange[1,padsize+1],:,:] = S(np.arange(1,padsize+1),:,:)
    outImg[np.arange[end() - padsize + 1,end()+1],:,:] = S(np.arange(end() - padsize + 1,end()+1),:,:)
    outImg[:,np.arange[1,padsize+1],:] = S(:,np.arange(1,padsize+1),:)
    outImg[:,np.arange[end() - padsize + 1,end()+1],:] = S(:,np.arange(end() - padsize + 1,end()+1),:)
    #figure(2); imshow([S, outImg],[]);
    return outImg
    
    return outImg