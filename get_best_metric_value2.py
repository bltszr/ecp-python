import numpy as np
    
def get_best_metric_value2(score = None,metric = None,img = None,kernel = None): 
    if 'MSE' == metric:
        val = np.abs(np.amin(score))
    else:
        if 'PSNR' == metric:
            val = np.abs(np.amax(score))
        else:
            if 'MSSIM' == metric:
                val = np.abs(np.amax(score))
            else:
                if 'MAD' == metric:
                    val = np.abs(np.amin(score))
                else:
                    if 'IFC' == metric:
                        val = np.abs(np.amax(score))
                    else:
                        if 'VIF' == metric:
                            val = np.abs(np.amax(score))
    
    return val
    return val