import numpy as np
    
def ksimi(f = None,kernel = None): 
    fnorm = f / norm(f,'fro')
    knorm = kernel / norm(kernel,'fro')
    kernelsimilarity = np.transpose(fnorm) * knorm
    return kernelsimilarity