import numpy as np
    
def blind_deconvBDF(y = None,lambda_dark = None,lambda_grad = None,opts = None): 
    
    # Do multi-scale blind deconvolution
    
    ## Input:
# @y : input blurred image (grayscale);
# @lambda_dark: the weight for the L0 regularization on intensity
# @lambda_grad: the weight for the L0 regularization on gradient
# @opts: see the description in the file "demo_text_deblurring.m"
## Output:
# @kernel: the estimated blur kernel
# @interim_latent: intermediate latent image
    
    # The Code is created based on the method described in the following paper
#   [1] Jinshan Pan, Deqing Sun, Hanspteter Pfister, and Ming-Hsuan Yang,
#        Blind Image Deblurring Using Dark Channel Prior, CVPR, 2016.
#   [2] Jinshan Pan, Zhe Hu, Zhixun Su, and Ming-Hsuan Yang,
#        Deblurring Text Images via L0-Regularized Intensity and Gradient
#        Prior, CVPR, 2014.
    
    #   Author: Jinshan Pan (sdluran@gmail.com)
#   Date  : 03/22/2016
    
    # gamma correct
    if opts.gamma_correct != 1:
        y = y ** opts.gamma_correct
    
    b = np.zeros((opts.kernel_size,opts.kernel_size))
    # set kernel size for coarsest level - must be odd
#minsize = max(3, 2*floor(((opts.kernel_size - 1)/16)) + 1);
#fprintf('Kernel size at coarsest level is #d\n', maxitr);
##
    ret = np.sqrt(0.5)
    ##
    maxitr = np.amax(int(np.floor(np.log(5 / np.amin(opts.kernel_size)) / np.log(ret))),0)
    num_scales = maxitr + 1
    print('Maximum iteration level is %d\n' % (num_scales))
    ##
    retv = ret ** np.array([np.arange(0,maxitr+1)])
    k1list = np.ceil(opts.kernel_size * retv)
    k1list = k1list + (np.mod(k1list,2) == 0)
    k2list = np.ceil(opts.kernel_size * retv)
    k2list = k2list + (np.mod(k2list,2) == 0)
    # derivative filters
    dx = np.array([[- 1,1],[0,0]])
    dy = np.array([[- 1,0],[1,0]])
    # blind deconvolution - multiscale processing
    for s in np.arange(num_scales,1+- 1,- 1).reshape(-1):
        if (s == num_scales):
            ##
# at coarsest level, initialize kernel
            ks = init_kernel(k1list(s))
            k1 = k1list(s)
            k2 = k1
        else:
            # upsample kernel from previous level to next finer level
            k1 = k1list(s)
            k2 = k1
            # resize kernel from previous level
            ks = resizeKer(ks,1 / ret,k1list(s),k2list(s))
        ###########################
        cret = retv(s)
        ys = downSmpImC(y,cret)
        print('Processing scale %d/%d; kernel size %dx%d; image size %dx%d\n' % (s,num_scales,k1,k2,ys.shape[1-1],ys.shape[2-1]))
        #-----------------------------------------------------------#
## Useless operation
        if (s == num_scales):
            __,__,threshold = threshold_pxpy_v1(ys,np.amax(ks.shape))
            ## Initialize the parameter: ???
#     if threshold<lambda_grad/10&&threshold~=0;
#         lambda_grad = threshold;
#         #lambda_dark = threshold_image_v1(ys);
#         lambda_dark = lambda_grad;
#     end
        #-----------------------------------------------------------#
        ks,lambda_dark,lambda_grad,interim_latent = blind_deconv_mainBDF(ys,ks,lambda_dark,lambda_grad,threshold,opts)
        # nameiter=num2str(s);
# imwrite(interim_latent,['C:\Users\yyy\Desktop\YYY\YYY\real\figure1\3_7_Interim latent image_',nameiter,'.png']);
## center the kernel
        ks = adjust_psf_center(ks)
        ks[ks < 0] = 0
        sumk = sum(ks)
        ks = ks / sumk
        ## set elements below threshold to 0
        if (s == 1):
            kernel = ks
            if opts.k_thresh > 0:
                kernel[kernel < np.amax[kernel] / opts.k_thresh] = 0
            else:
                kernel[kernel < 0] = 0
            kernel = kernel / sum(kernel)
    
    ## end kernel estimation
    return kernel,interim_latent
    
    ## Sub-function
    
def init_kernel(minsize = None): 
    k = np.zeros((minsize,minsize))
    k[[minsize - 1] / 2,np.arange[[minsize - 1] / 2,[minsize - 1] / 2 + 1+1]] = 1 / 2
    return k
    
    ##
    
def downSmpImC(I = None,ret = None): 
    ## refer to Levin's code
    if (ret == 1):
        sI = I
        return sI
    
    ###################
    
    sig = 1 / pi * ret
    g0 = np.array([np.arange(- 50,50+1)]) * 2 * pi
    sf = np.exp(- 0.5 * g0 ** 2 * sig ** 2)
    sf = sf / sum(sf)
    csf = cumsum(sf)
    csf = np.amin(csf,csf(np.arange(end(),1+- 1,- 1)))
    ii = find(csf > 0.05)
    sf = sf(ii)
    sum(sf)
    I = conv2(sf,np.transpose(sf),I,'valid')
    gx,gy = np.meshgrid(np.array([np.arange(1,I.shape[2-1]+1 / ret,1 / ret)]),np.array([np.arange(1,I.shape[1-1]+1 / ret,1 / ret)]))
    sI = interp2(I,gx,gy,'bilinear')
    return sI
    
    ##
    
def resizeKer(k = None,ret = None,k1 = None,k2 = None): 
    ##
# levin's code
    k = imresize(k,ret)
    k = np.amax(k,0)
    k = fixsize(k,k1,k2)
    if np.amax(k) > 0:
        k = k / sum(k)
    
    return k
    
    ##
    
def fixsize(f = None,nk1 = None,nk2 = None): 
    k1,k2 = f.shape
    while (np.logical_or((k1 != nk1),(k2 != nk2))):

        if (k1 > nk1):
            s = np.sum(f, 2-1)
            if (s(1) < s(end())):
                f = f(np.arange(2,end()+1),:)
            else:
                f = f(np.arange(1,end() - 1+1),:)
        if (k1 < nk1):
            s = np.sum(f, 2-1)
            if (s(1) < s(end())):
                tf = np.zeros((k1 + 1,f.shape[2-1]))
                tf[np.arange[1,k1+1],:] = f
                f = tf
            else:
                tf = np.zeros((k1 + 1,f.shape[2-1]))
                tf[np.arange[2,k1 + 1+1],:] = f
                f = tf
        if (k2 > nk2):
            s = np.sum(f, 1-1)
            if (s(1) < s(end())):
                f = f(:,np.arange(2,end()+1))
            else:
                f = f(:,np.arange(1,end() - 1+1))
        if (k2 < nk2):
            s = np.sum(f, 1-1)
            if (s(1) < s(end())):
                tf = np.zeros((f.shape[1-1],k2 + 1))
                tf[:,np.arange[1,k2+1]] = f
                f = tf
            else:
                tf = np.zeros((f.shape[1-1],k2 + 1))
                tf[:,np.arange[2,k2 + 1+1]] = f
                f = tf
        k1,k2 = f.shape

    
    nf = f
    return nf
    
    ##
    return kernel,interim_latent