#Program for Peak Signal to Noise Ratio Calculation

#Author : Athi Narayanan S
#M.E, Embedded Systems,
#K.S.R College of Engineering
#Erode, Tamil Nadu, India.
#http://sites.google.com/site/athisnarayanan/
#s_athi1983@yahoo.co.in

import numpy as np
    
def caclpsnr(origImg = None,distImg = None): 
    origImg = double(origImg)
    distImg = double(distImg)
    M,N = origImg.shape
    error = origImg - distImg
    MSE = sum(sum(np.multiply(error,error))) / (M * N)
    if (MSE > 0):
        PSNR = 10 * np.log(255 * 255 / MSE) / np.log(10)
    else:
        PSNR = 99
    
    return PSNR