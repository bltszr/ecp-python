import numpy as np
import numpy.matlib
    
def L0Restoration(Im = None,kernel = None,lambda_ = None,kappa = None): 
    ##
# Image restoration with L0 prior
# The objective function:
# S^* = argmin ||I*k - B||^2 + lambda |\nabla I|_0
## Input:
# @Im: Blurred image
# @kernel: blur kernel
# @lambda: weight for the L0 prior
# @kappa: Update ratio in the ADM
## Output:
# @S: Latent image
    
    # The Code is created based on the method described in the following paper
#   [1] Jinshan Pan, Zhe Hu, Zhixun Su, and Ming-Hsuan Yang,
#        Deblurring Text Images via L0-Regularized Intensity and Gradient
#        Prior, CVPR, 2014.
#   [2] Li Xu, Cewu Lu, Yi Xu, and Jiaya Jia. Image smoothing via l0 gradient minimization.
#        ACM Trans. Graph., 30(6):174, 2011.
    
    #   Author: Jinshan Pan (sdluran@gmail.com)
#   Date  : 05/18/2014
    
    if not ('kappa' is not None) :
        kappa = 2.0
    
    ## pad image
    H = Im.shape[1-1]
    W = Im.shape[2-1]
    Im = wrap_boundary_liu(Im,opt_fft_size(np.array([H,W]) + kernel.shape - 1))
    ##
    S = Im
    betamax = 100000.0
    fx = np.array([1,- 1])
    fy = np.array([[1],[- 1]])
    N,M,D = Im.shape
    sizeI2D = np.array([N,M])
    otfFx = psf2otf(fx,sizeI2D)
    otfFy = psf2otf(fy,sizeI2D)
    ##
    KER = psf2otf(kernel,sizeI2D)
    Den_KER = np.abs(KER) ** 2
    ##
    Denormin2 = np.abs(otfFx) ** 2 + np.abs(otfFy) ** 2
    if D > 1:
        Denormin2 = np.matlib.repmat(Denormin2,np.array([1,1,D]))
        KER = np.matlib.repmat(KER,np.array([1,1,D]))
        Den_KER = np.matlib.repmat(Den_KER,np.array([1,1,D]))
    
    Normin1 = np.multiply(conj(KER),fft2(S))
    ##
    beta = 2 * lambda_
    while beta < betamax:

        Denormin = Den_KER + beta * Denormin2
        h = np.array([np.diff(S,1,2),S(:,1,:) - S(:,end(),:)])
        v = np.array([[np.diff(S,1,1)],[S(1,:,:) - S(end(),:,:)]])
        if D == 1:
            t = (h ** 2 + v ** 2) < lambda_ / beta
        else:
            t = np.sum((h ** 2 + v ** 2), 3-1) < lambda_ / beta
            t = np.matlib.repmat(t,np.array([1,1,D]))
        h[t] = 0
        v[t] = 0
        Normin2 = np.array([h(:,end(),:) - h(:,1,:),- np.diff(h,1,2)])
        Normin2 = Normin2 + np.array([[v(end(),:,:) - v(1,:,:)],[- np.diff(v,1,1)]])
        FS = (Normin1 + beta * fft2(Normin2)) / Denormin
        S = real(ifft2(FS))
        beta = beta * kappa

    
    S = S(np.arange(1,H+1),np.arange(1,W+1),:)
    return S
    
    return S