import numpy as np
    
def deblurring_adm_aniso(B = None,k = None,lambda_ = None,alpha = None): 
    # Solving TV-\ell^2 deblurring problem via ADM/Split Bregman method
    
    # This reference of this code is :Fast Image Deconvolution using Hyper-Laplacian Priors
# Original code is created by Dilip Krishnan
# Finally modified by Jinshan Pan 2011/12/25
# Note:
# In this model, aniso TV regularization method is adopted.
# Thus, we do not use the Lookup table method proposed by Dilip Krishnan and Rob Fergus
# Reference: Kernel Estimation from Salient Structure for Robust Motion
# Deblurring
#Last update: (2012/6/20)
    beta = 1 / lambda_
    beta_rate = 2 * np.sqrt(2)
    #beta_max = 5*2^10;
    beta_min = 0.001
    m,n = B.shape
    # initialize with input or passed in initialization
    I = B
    # make sure k is a odd-sized
    if (np.logical_or((np.mod(k.shape[1-1],2) != 1),(np.mod(k.shape[2-1],2) != 1))):
        print('Error - blur kernel k must be odd-sized.\n' % ())
        return I
    
    Nomin1,Denom1,Denom2 = computeDenominator(B,k)
    Ix = np.array([np.diff(I,1,2),I(:,1) - I(:,n)])
    Iy = np.array([[np.diff(I,1,1)],[I(1,:) - I(m,:)]])
    ## Main loop
    while beta > beta_min:

        gamma = 1 / (2 * beta)
        Denom = Denom1 + gamma * Denom2
        # subproblem for regularization term
        if alpha == 1:
            Wx = np.multiply(np.amax(np.abs(Ix) - beta * lambda_,0),np.sign(Ix))
            Wy = np.multiply(np.amax(np.abs(Iy) - beta * lambda_,0),np.sign(Iy))
            ##
        else:
            Wx = solve_image(Ix,1 / (beta * lambda_),alpha)
            Wy = solve_image(Iy,1 / (beta * lambda_),alpha)
        Wxx = np.array([Wx(:,n) - Wx(:,1),- np.diff(Wx,1,2)])
        Wxx = Wxx + np.array([[Wy(m,:) - Wy(1,:)],[- np.diff(Wy,1,1)]])
        Fyout = (Nomin1 + gamma * fft2(Wxx)) / Denom
        I = real(ifft2(Fyout))
        # update the gradient terms with new solution
        Ix = np.array([np.diff(I,1,2),I(:,1) - I(:,n)])
        Iy = np.array([[np.diff(I,1,1)],[I(1,:) - I(m,:)]])
        beta = beta / 2

    
    ###################################################
    
def computeDenominator(y = None,k = None): 
    
    # computes denominator and part of the numerator for Equation (3) of the
# paper
    
    # Inputs:
#  y: blurry and noisy input
#  k: convolution kernel
    
    # Outputs:
#      Nomin1  -- F(K)'*F(y)
#      Denom1  -- |F(K)|.^2
#      Denom2  -- |F(D^1)|.^2 + |F(D^2)|.^2
    
    sizey = y.shape
    otfk = psf2otf(k,sizey)
    Nomin1 = np.multiply(conj(otfk),fft2(y))
    Denom1 = np.abs(otfk) ** 2
    # if higher-order filters are used, they must be added here too
    Denom2 = np.abs(psf2otf(np.array([1,- 1]),sizey)) ** 2 + np.abs(psf2otf(np.array([[1],[- 1]]),sizey)) ** 2
    return I