import numpy as np
import numpy.matlib
    
def L0Deblur_dark_chanelBD(Im = None,kernel = None,lambda_ = None,wei_grad = None,kappa = None): 
    ##
# Image restoration with L0 regularized intensity and gradient prior
# The objective function:
# S = argmin ||I*k - B||^2 + \lambda |D(I)|_0 + wei_grad |\nabla I|_0
## Input:
# @Im: Blurred image
# @kernel: blur kernel
# @lambda: weight for the L0 intensity prior
# @wei_grad: weight for the L0 gradient prior
# @kappa: Update ratio in the ADM
## Output:
# @S: Latent image
    
    # The Code is created based on the method described in the following paper
#   [1] Jinshan Pan, Deqing Sun, Hanspteter Pfister, and Ming-Hsuan Yang,
#        Blind Image Deblurring Using Dark Channel Prior, CVPR, 2016.
    
    if not ('kappa' is not None) :
        kappa = 2.0
    
    ## pad image
# H = size(Im,1);    W = size(Im,2);
# Im = wrap_boundary_liu(Im, opt_fft_size([H W]+size(kernel)-1));
##
    S = Im
    betamax = 100000.0
    fx = np.array([1,- 1])
    fy = np.array([[1],[- 1]])
    if len(Im.shape) == 3:    
        N,M,D = Im.shape
    else:
        N,M = Im.shape
        D = 1

    sizeI2D = np.array([N,M])
    otfFx = psf2otf(fx,sizeI2D)
    otfFy = psf2otf(fy,sizeI2D)
    ##
    KER = psf2otf(kernel,sizeI2D)
    Den_KER = np.abs(KER) ** 2
    ##
    Denormin2 = np.abs(otfFx) ** 2 + np.abs(otfFy) ** 2
    if D > 1:
        Denormin2 = np.matlib.repmat(Denormin2,np.array([1,1,D]))
        KER = np.matlib.repmat(KER,np.array([1,1,D]))
        Den_KER = np.matlib.repmat(Den_KER,np.array([1,1,D]))
    
    Normin1 = np.multiply(conj(KER),fft2(S))
    ## pixel sub-problem
##
    dark_r = 45
    
    #mybeta_pixel = 2*lambda;
#[J, J_idx] = dark_channel(S, dark_r);
    mybeta_pixel = lambda_ / (graythresh((S) ** 2))
    maxbeta_pixel = 2 ** 3
    
    while mybeta_pixel < maxbeta_pixel:

        ##
        J,J_idx = dark_channel(S,dark_r)
        u = J
        if D == 1:
            t = u ** 2 < lambda_ / mybeta_pixel
        else:
            t = np.sum(u ** 2, 3-1) < lambda_ / mybeta_pixel
            t = np.matlib.repmat(t,np.array([1,1,D]))
        u[t] = 0
        clear('t')
        u = assign_dark_channel_to_pixel(S,u,J_idx,dark_r)
        BS = 1 - S
        BJ,BJ_idx = dark_channel(BS,dark_r)
        bu = BJ
        if D == 1:
            t = bu ** 2 < lambda_ / mybeta_pixel
        else:
            t = np.sum(bu ** 2, 3-1) < lambda_ / mybeta_pixel
            t = np.matlib.repmat(t,np.array([1,1,D]))
        bu[t] = 0
        bu = assign_dark_channel_to_pixel(BS,bu,BJ_idx,dark_r)
        ## Gradient sub-problem
        beta = 2 * wei_grad
        #beta = 0.01;
        while beta < betamax:

            Denormin = Den_KER + beta * Denormin2 + 2 * mybeta_pixel
            h = np.array([np.diff(S,1,2), S[:,0,:] - S[:,-1,:]])
            v = np.array([[np.diff(S,1,1)], 
                          [S[0,:,:] - S[-1,:,:]]])
            if D == 1:
                t = (h ** 2 + v ** 2) < wei_grad / beta
            else:
                t = np.sum((h ** 2 + v ** 2), 3-1) < wei_grad / beta
                t = np.matlib.repmat(t,np.array([1,1,D]))
            h[t] = 0
            v[t] = 0
            clear('t')
            Normin2 = np.array([h[:,-1,:] - h[:,0,:],- np.diff(h,1,2)])
            Normin2 = Normin2 + np.array([[v[-1,:,:] - v[0,:,:]],
                                          [-np.diff(v,1,1)]])
            FS = (Normin1 + beta * fft2(Normin2) + mybeta_pixel * fft2(u) + mybeta_pixel * fft2(1 - bu)) / Denormin
            S = real(ifft2(FS))
            ##
            beta = beta * kappa
            if wei_grad == 0:
                break

        mybeta_pixel = mybeta_pixel * kappa
    return S
