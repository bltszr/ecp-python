from staticfg import CFGBuilder

cfg = CFGBuilder().build_from_file('demo_deblurring.py', './pycode/demo_deblurring.py')
cfg.build_visual('demo_deblurring', 'pdf')

