import numpy as np
import numpy.matlib
    
def bilateral_filter(img = None,sigma_s = None,sigma = None,boundary_method = None,s_size = None): 
    if not ('boundary_method' is not None) :
        boundary_method = 'replicate'
    
    if isinteger(img) == 1:
        img = single(img) / 255
    
    h,w,d = img.shape
    if d == 3:
        C = makecform('srgb2lab')
        lab = single(applycform(double(img),C))
        sigma = sigma * 100
    else:
        lab = img
        sigma = sigma * np.sqrt(d)
    
    if ('s_size' is not None):
        fr = s_size
    else:
        fr = np.ceil(sigma_s * 3)
    
    p_img = padarray(img,np.array([fr,fr]),boundary_method)
    p_lab = padarray(lab,np.array([fr,fr]),boundary_method)
    u = fr + 1
    b = u + h - 1
    l = fr + 1
    r = l + w - 1
    r_img = np.zeros(h,w,d,'single')
    w_sum = np.zeros((h,w,'single'))
    spatial_weight = fspecial('gaussian',2 * fr + 1,sigma_s)
    ss = sigma * sigma
    for y in np.arange(- fr,fr+1).reshape(-1):
        for x in np.arange(- fr,fr+1).reshape(-1):
            w_s = spatial_weight(y + fr + 1,x + fr + 1)
            n_img = p_img[np.arange(u + y,b + y+1),np.arange(l + x,r + x+1),:]
            n_lab = p_lab[np.arange(u + y,b + y+1),np.arange(l + x,r + x+1),:]
            f_diff = lab - n_lab
            f_dist = np.sum(f_diff ** 2, 3-1)
            w_f = np.exp(- 0.5 * (f_dist / ss))
            w_t = np.multiply(w_s,w_f)
            r_img = r_img + np.multiply(n_img,np.matlib.repmat(w_t,np.array([1,1,d])))
            w_sum = w_sum + w_t
    
    r_img = r_img / np.matlib.repmat(w_sum,np.array([1,1,d]))
    return r_img
    
    return r_img
