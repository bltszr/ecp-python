# =================================================================
# DEFINE IMGPATH, DEBLNAME, IMGTEXT AND metric
import numpy as np
clear('all')
# -----------------------------------------------------------------
# MYPATH = ['/is/ei/rolfk/cpr/prj/camera_shake_benchmark/code/' ...
#     'eval_deblur_img'];
MYPATH = np.array(['C:\Users\yyy\Desktop\YYY\'])
DEBLPATH = 'C:\Users\yyy\Desktop\YYY\ECCV\dataset_results_eccv12'
DEBLNAME = 'Blurry'
IMGEXT = '_out_6865s.png'
# -----------------------------------------------------------------
# DEFINE METRIC: available metrics: 'MSE','MSSIM','VIF','IFC','PSNR','MAD'
# -----------------------------------------------------------------
metric = np.array(['PSNR'])
# =================================================================
# DEFINE image Number (1 to 4) and Kernel Number (1 to 12) of
# deblurred images, which shall be assigned a score
# -----------------------------------------------------------------
imgNo = np.arange(1,4+1)
kernNo = np.arange(1,12+1)
# do not calculate the score for following images [img, kernel; img
# kernel]
# exclude = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6];
exclude = []
# =================================================================
# run the evaluation script with all ground truth images
# -----------------------------------------------------------------
cd(MYPATH)
for iM in np.arange(1,len(metric)+1).reshape(-1):
    for iImg in 2.reshape(-1):
        for iKern in np.arange(9,10+1).reshape(-1):
            #       if ~isempty(intersect([iImg iKern],exclude,'rows'))
#         continue
#       end
            metricNow = metric[iM]
            deblurred = imread(sprintf('%s/%s%d_%d%s',DEBLPATH,DEBLNAME,iImg,iKern,IMGEXT))
            scores = eval_image(deblurred,iImg,iKern,metricNow)
            if 'MSE' == metricNow:
                DeblurScore.MSE[iImg,iKern] = get_best_metric_value2(scores.MSE,'MSE',iImg,iKern)
            else:
                if 'MSSIM' == metricNow:
                    DeblurScore.MSSIM[iImg,iKern] = get_best_metric_value2(scores.MSSIM,'MSSIM',iImg,iKern)
                else:
                    if 'VIF' == metricNow:
                        DeblurScore.VIF[iImg,iKern] = get_best_metric_value2(scores.VIF,'VIF',iImg,iKern)
                    else:
                        if 'IFC' == metricNow:
                            DeblurScore.IFC[iImg,iKern] = get_best_metric_value2(scores.IFC,'IFC',iImg,iKern)
                        else:
                            if 'PSNR' == metricNow:
                                DeblurScore.PSNR[iImg,iKern] = get_best_metric_value2(scores.PSNR,'PSNR',iImg,iKern)
                            else:
                                if 'MAD' == metricNow:
                                    DeblurScore.MAD[iImg,iKern] = get_best_metric_value2(scores.MAD,'MAD',iImg,iKern)

# for k=1:12
#     PSNRDCP(5,k)=mean(PSNRDCP(1:4,k));
#     PSNRBDCP(5,k)=mean(PSNRBDCP(1:4,k));
# end

# =================================================================
# function dependencies
# -----------------------------------------------------------------
# start_eval_image.m
#   *eval_image.m
#     -dftregistration.m
#     -imshift.m
#     -metrix_mux
#     -MAD_index
#       + myrgb2gray
#   *get_best_metric_value2