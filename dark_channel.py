import numpy as np
    
def dark_channel(I = None,patch_size = None): 
    # function J = dark_channel(I, patch_size);
    
    # Computes the "Dark Channel" of corresponding RGB image.
# -Finds from the input image the minimum value among all
#  pixels within the patch centered around the location of the
#  target pixel in the newly created dark channel image 'J'
#  J is a 2-D image (grayscale).
    
    # Example: J = dark_channel(I, 15); # computes using 15x15 patch
    
    # Check to see that the input is a color image
# if ndims(I) == 3
#     [M N C] = size(I);
#     J = zeros(M, N); # Create empty matrix for J
#     J_index = zeros(M, N); # Create empty index matrix
# else
#     error('Sorry, dark_channel supports only RGB images');
# end
## for grayscale image
    
    M,N,C = I.shape
    J = np.zeros((M,N))
    
    J_index = np.zeros((M,N))
    
    # Test if patch size has odd number
    if not np.mod(np.asarray(patch_size).size,2) :
        raise Exception('Invalid Patch Size: Only odd number sized patch supported.')
    
    # pad original image
#I = padarray(I, [floor(patch_size./2) floor(patch_size./2)], 'symmetric');
    I = padarray(I,np.array([int(np.floor(patch_size / 2)),int(np.floor(patch_size / 2))]),'replicate')
    # Compute the dark channel
    m = np.arange(1,M+1)
    for n in np.arange(1,N+1).reshape(-1):
        patch = I(np.arange(m,(m + patch_size - 1)+1),np.arange(n,(n + patch_size - 1)+1),:)
        tmp = np.amin(patch,[],3)
        tmp_val,tmp_idx = np.amin(tmp)
        J[m,n] = tmp_val
        J_index[m,n] = tmp_idx
    
    return J,J_index
    
    return J,J_index