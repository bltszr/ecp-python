import numpy as np
    
def comp_upto_shift1(I1 = None,I2 = None): 
    #function [ssde,tI1]=comp_upto_shift(I1,I2)
#  compute sum of square differences between two images, after
#  finding the best shift between them. need to account for shift
#  because the kernel reconstruction is shift invariant- a small
#  shift of the image and kernel will not effect the likelihood score.
#Input:
#I1,I2-images to compare
#Output:
#ssde-sum of square differences
#tI1-image I1 at best shift toward I2
    
    #Writen by: Anat Levin, anat.levin@weizmann.ac.il (c)
    
    N1,N2 = I1.shape
    maxshift = 5
    shifts = np.array([np.arange(- 5,5+0.25,0.25)])
    I2 = I2(np.arange(16,end() - 15+1),np.arange(16,end() - 15+1))
    I1 = I1(np.arange(16 - maxshift,end() - 15 + maxshift+1),np.arange(16 - maxshift,end() - 15 + maxshift+1))
    N1,N2 = I2.shape
    gx,gy = np.meshgrid(np.array([np.arange(1 - maxshift,N2 + maxshift+1)]),np.array([np.arange(1 - maxshift,N1 + maxshift+1)]))
    gx0,gy0 = np.meshgrid(np.array([np.arange(1,N2+1)]),np.array([np.arange(1,N1+1)]))
    for i in np.arange(1,len(shifts)+1).reshape(-1):
        for j in np.arange(1,len(shifts)+1).reshape(-1):
            gxn = gx0 + shifts(i)
            gyn = gy0 + shifts(j)
            tI1 = interp2(gx,gy,I1,gxn,gyn)
            ssdem[i,j] = sum(sum((tI1 - I2) ** 2))
    
    ssde = np.amin(ssdem)
    i,j = find(ssdem == ssde)
    gxn = gx0 + shifts(i)
    gyn = gy0 + shifts(j)
    tI1 = interp2(gx,gy,I1,gxn,gyn)
    return ssde,tI1